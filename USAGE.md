# Utilisation

## Installation

```bash
poetry install
```

## Génération des données

### Récupérer les adresses

```bash
poetry run python gametro_collecte_dechets/collect.py adresses.txt [--auto]
```

Saisir `y` pour parser un fichier (tous les fichiers PDFs ne sont pas à parser), et saisir la ville associée.
Pour chaque PDF, une confirmation est affichée pour vérifier que les bonnes adresses ont été parsées - si ce n'est pas le cas, la page suivante sera parsée.

Le fichier résultant nécessite du post-traitement:

* Pour corriger les villes Domène/Murianette et Eybens/Poisat
* Pour mettre pour toutes les lignes au format `<Nom>, <Numéro de rue>, <Rue>, <Ville>` (ou, à défaut, `<Nom>, <Rue>, <Ville>`)
* Pour corriger du mauvais parsing:

```plain
La Fournée, Grenoble
cours de la Libération 5 
-->
La Fournée, 5 cours de la Libération, Grenoble
Bibliothèque Eaux-Claires Mistral, 49 rue des Eaux Claires, Grenoble
```

```plain
La Talemelerie 12 Place Sainte-Claire, Grenoble
1 Place de l'Etoile, Grenoble
4 Place Championnet, Grenoble
-->
La Talemelerie 12 Place Sainte-Claire, Grenoble
La Talemelerie 1 Place de l'Etoile, Grenoble
La Talemelerie 4 Place Championnet, Grenoble
```

* Pour faire matcher les noms inscrits avec ceux d'OpenStreetMap.

Le fichier CSV `fixups.csv` permet de stocker ces post-traitements.

### Récupérer les points OSM

```bash
poetry run python gametro_collecte_dechets/locate.py adresses.txt public/locations.geojson [--debug]
```

En cas de non-détection d'une adresse, si existante dans OSM, la modifier dans le fichier `fixups.csv` (le nom, la rue et la ville doivent correspondre exactement).

### Mise à jour de la carte

Editer https://umap.openstreetmap.fr/fr/map/points-de-retrait-des-sacs-compostables_599880

### Générer le site

```bash
poetry run python gametro_collecte_dechets/generate_static.py
```

### Mettre à jour la carte

Editer la carte sur [uMap](https://umap.openstreetmap.fr/fr/map/points-de-retrait-des-sacs-compostables_599880) en important le fichier GeoJSON dans le calque "Points de retraits" (réservé aux éditeurs).

## Checks

* Linting: `poetry run flake8 gametro_collecte_dechets tests && poetry run mypy gametro_collecte_dechets tests && poetry run black --check gametro_collecte_dechets tests`
* Tests: `poetry run pytest`
