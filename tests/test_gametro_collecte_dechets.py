import os

import toml

from gametro_collecte_dechets import __version__


def test_version():
    basedir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

    conf = toml.load(os.path.join(basedir, "pyproject.toml"))
    assert __version__ == conf["tool"]["poetry"]["version"]
