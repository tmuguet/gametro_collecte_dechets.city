import re
from io import BytesIO
from typing import List, TextIO

import click
import pdfplumber
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm


def get_links() -> List[str]:
    links = set()
    r = requests.get("https://www.grenoblealpesmetropole.fr/873-la-collecte-en-porte-a-porte.htm")
    soup = BeautifulSoup(r.text, "html.parser")

    for link in soup.find_all("a"):
        if link.get("class", [""])[0] == "document" and link.get("href").endswith(".pdf"):
            links.add("https://www.grenoblealpesmetropole.fr{}".format(link.get("href")))

    return sorted(links)


def _fix_name(name: str) -> str:
    addr = name.replace(" : ", ", ").replace("’", "'")
    addr = re.sub(
        r"(\d+)", r", \1, ", addr
    )  # On rajoute des virgules autour du numéro de la rue (potentiels effets de bord, mais + rentable dans ce sens)
    addr = re.sub(r" +", " ", addr)  # On supprime tous espaces surnuméraires
    addr = re.sub(r" *,", ",", addr)  # On supprime tous espaces avant une virgule
    addr = re.sub(r",+", ",", addr)  # On supprime toutes virgules surnuméraires

    addr = addr.replace(", 8, Mai, 1945,", " 8 Mai 1945 ")
    addr = addr.replace(", bis ", "bis, ")

    addr = addr.strip()
    return addr


def _has_way(s: str) -> bool:
    _s = s.lower()
    return (
        "rue" in _s
        or "boulevard" in _s
        or "avenue" in _s
        or "place" in _s
        or "mail" in _s
        or "cours" in _s
        or "chemin" in _s
    )


def _get_addresses(page) -> List[str]:
    text = page.extract_words(use_text_flow=True)
    texts = [x["text"] for x in text]

    selected = False
    currentIndex = 0
    addresses = []
    currentAddress = ""
    for t in texts:
        if t in ["RENSEIGNEMENTS", "JOURS", "Conception", "*Seuls"] and selected:
            break

        try:
            if int(t) == currentIndex + 1:
                # Nouvel item dans le PDF

                if (
                    selected
                    and len(currentAddress) > 1
                    and len(currentAddress) < 30
                    and not _has_way(currentAddress)
                ):
                    # C'est en fait un numéro de rue
                    currentAddress += t + " "
                    continue

                if currentAddress:
                    addresses.append(_fix_name(currentAddress))
                selected = True
                currentAddress = ""
                currentIndex += 1
                continue
        except ValueError:
            pass

        if selected:
            currentAddress += t + " "

    if currentAddress:
        addresses.append(_fix_name(currentAddress))
    return addresses


def get_addresses(url: str, city: str, auto: bool) -> List[str]:
    r = requests.get(url)

    pdf = pdfplumber.open(BytesIO(r.content))
    pages = pdf.pages
    for idx, page in enumerate(reversed(pages)):
        print(f"Parsing page {len(pages)-idx}...")
        addresses = _get_addresses(page)
        if addresses and len(addresses) > 1:
            if auto:
                print(f"Got '{addresses[0]}' ... '{addresses[-1]}'.")
                break
            else:
                s = input(f"Got '{addresses[0]}' ... '{addresses[-1]}'. Correct? (y/[n]) ")
                if s == "y":
                    break

    pdf.close()

    return [f"{x}, {city}" for x in addresses]


@click.command()
@click.argument("outputfile", type=click.File("w"))
@click.option(
    "-a",
    "--auto",
    is_flag=True,
    default=False,
    help="Mode de détection automatique des fichiers à parser et du parsing de ces fichiers",
)
def collect(outputfile: TextIO, auto: bool):
    """Collecte les adresses à partir du site de la Métro et génère un fichier texte"""
    for link in tqdm(get_links(), "Liens"):
        default_do_parse = (
            "Pont-de-Claix" in link
            or "Claix" in link
            or "Domene" in link
            or "Echirolles" in link
            or "Eybens" in link
            or "Fontaine" in link
            or "Gieres" in link
            or "Grenoble" in link
            or "Saint-Martin-d-Heres" in link
            or "Sassenage" in link
            or "Seyssinet" in link
            or "Seyssins" in link
        )
        if auto:
            s = ""
        else:
            t = "[y]/n" if default_do_parse else "y/[n]"
            s = input(f"Parse {link}? ({t}) ")

        if s == "y" or (default_do_parse and s == ""):
            default_city = (
                "Le Pont-de-Claix"
                if "Pont-de-Claix" in link
                else "Claix"
                if "Claix" in link
                else "Domène"
                if "Domene" in link
                else "Échirolles"
                if "Echirolles" in link
                else "Eybens"
                if "Eybens" in link
                else "Fontaine"
                if "Fontaine" in link
                else "Gières"
                if "Gieres" in link
                else "Grenoble"
                if "Grenoble" in link
                else "Saint-Martin-d'Hères"
                if "Saint-Martin-d-Heres" in link
                else "Sassenage"
                if "Sassenage" in link
                else "Seyssinet-Pariset"
                if "Seyssinet" in link
                else "Seyssins"
                if "Seyssins" in link
                else ""
            )
            if auto:
                city = ""
            else:
                city = input(f"City? ([{default_city}]) ")

            if not city:
                city = default_city

            if auto:
                print(f"Parsing {link} ({default_city})")

            outputfile.write("\n".join(get_addresses(link, city, auto)) + "\n")
            outputfile.flush()
        elif auto:
            print(f"Ignoring {link}")


if __name__ == "__main__":
    collect()
