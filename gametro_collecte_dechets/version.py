import os

import toml

basedir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

conf = toml.load(os.path.join(basedir, "pyproject.toml"))
version = conf["tool"]["poetry"]["version"]
