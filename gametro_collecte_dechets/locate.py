import csv
import json
from typing import Any, Callable, Dict, Optional, TextIO, Tuple

import click
import requests
from tqdm import tqdm


def search_name(addr: str) -> Optional[Dict[str, Any]]:
    p = requests.get(
        "https://nominatim.openstreetmap.org/search",
        params={
            "q": f"{addr}, france",
            "format": "json",
            "addressdetails": "1",
            "limit": "1",
        },
    )
    res = p.json()
    if res:
        return res[0]
    else:
        return None


def _readline(line: str) -> Tuple[str, Optional[str]]:
    addr = line.strip()
    comment: Optional[str] = None
    if "#" in addr:
        addr, comment = [x.strip() for x in addr.split("#")]
    return addr, comment


@click.command()
@click.argument("inputfile", type=click.File("r"))
@click.argument("outputfile", type=click.File("w"))
@click.option("-d", "--debug", is_flag=True, default=False)
def locate(inputfile: TextIO, outputfile: TextIO, debug: bool):
    """Recherche à partir d'un fichier les adresses sur OpenStreetMap et génère le fichier GeoJSON associé"""

    if debug:
        func: Callable[[Any], Any] = input
    else:
        func = print

    fixups = {}
    with open("fixups.csv") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=";")
        for row in csvreader:
            fixups[row[0]] = row[1]

    ignored = set()
    with open("ignored.txt") as txtfile:
        for line in txtfile.readlines():
            ignored.add(_readline(line)[0])

    features = []
    for line in tqdm(inputfile.readlines()):
        addr, comment = _readline(line)
        if comment == "IGNORE":
            continue

        if addr in fixups:
            addr = fixups[addr]

        if addr in ignored:
            continue

        location = search_name(addr)
        if not location:
            func(f"Could not find location for '{addr}'")
        else:
            city = (
                location["address"].get("town")
                or location["address"].get("city")
                or location["address"].get("village")
            )
            if not city.lower() == addr.split(",")[-1].strip().lower():
                func(f"Warning: wrong city: '{addr}' gave '{city}'")
            elif not location["display_name"].lower().startswith(addr.split(",")[0].strip().lower()):
                func(f"Warning: wrong name: '{addr}' gave '{location['display_name']}'")

            name = location["address"][location["class"]]
            address = location["address"].get("house_number", "") + " " + location["address"]["road"]

            if location["class"] == "amenity" and location["type"] == "townhall" and name == city:
                name = f"Mairie de {city}"

            display = f"{name}, {address}, {city}"
            if comment:
                display += f" ({comment})"

            feature = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [float(location["lon"]), float(location["lat"])],
                },
                "properties": {
                    "name": display,
                    "full_name": location["display_name"],
                    "osm_type": location["osm_type"],
                    "osm_id": int(location["osm_id"]),
                    "class": location["class"],
                    "type": location["type"],
                    "address": location["address"],
                    "comment": comment,
                },
            }
            features.append(feature)

    outputfile.write(json.dumps({"type": "FeatureCollection", "features": features}))


if __name__ == "__main__":
    locate()
