import datetime
import os
import subprocess

import click
from jinja2 import Environment, FileSystemLoader, select_autoescape

from gametro_collecte_dechets.version import version


@click.command()
def generate():
    """Génère le site statique"""

    basedir = os.path.abspath(os.path.dirname(__file__))
    rootdir = os.path.dirname(basedir)
    sha1 = subprocess.check_output(["git", "describe", "--always"]).strip().decode()

    env = Environment(
        loader=FileSystemLoader(os.path.join(basedir, "templates")),
        autoescape=select_autoescape(["html", "xml"]),
    )

    for f in ["index.html"]:
        template = env.get_template(f)
        with open(os.path.join(rootdir, "public", f), "w") as fp:
            fp.write(template.render(version=f"{version}@{sha1}", today=datetime.date.today()))


if __name__ == "__main__":
    generate()
